import java.util.*;
public class Task2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		System.out.println("Message: ");
		String message = input.nextLine();
		System.out.println("Number of shifts:");
		int shift = input.nextInt();
		String encryption = "";
		char alphabet;
		
		for (int i = 0; i < message.length(); i++) {
			// Shift one character at a time
			alphabet = message.charAt(i);

			// if alphabet lies between a and z
			if (alphabet >= 'a' && alphabet <= 'z') {
				// shift alphabet
				alphabet = (char) (alphabet + shift);
				// if shifted alphabet greater than 'z'
				if (alphabet > 'z') {
					// reshift to starting position
					alphabet = (char) (alphabet + 'a' - 'z' - 1);
				}
				encryption = encryption + alphabet;
			}

			// if alphabet lies between 'A'and 'Z'
			else if (alphabet >= 'A' && alphabet <= 'Z') {
				// shift alphabet
				alphabet = (char) (alphabet + shift);

				// if shifted alphabet greater than 'Z'
				if (alphabet > 'Z') {
					// reshift to starting position
					alphabet = (char) (alphabet + 'A' - 'Z' - 1);
				}
				encryption = encryption + alphabet;
			} else {
				encryption = encryption + alphabet;
			}
		}
		
		System.out.println("Encrypted message: " + encryption);

	}

}
