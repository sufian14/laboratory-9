
public class Task1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int n = 1000000; // number of vertices
		int k = 0; // number of vertices inside the circle
		double x, y; // coordinates

		for (int i = 0; i < n; i++) {
			x = Math.random();
			y = Math.random();
			if (x * x + y * y <= 1) // check if the vertex is inside the circle
				k++;
		}

		System.out.println("pi = " + 4 * (double) k / (double) n);

	}

}
